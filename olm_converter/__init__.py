from pathlib import Path
# from queue import Queue
from typing import List, BinaryIO, Iterable, NamedTuple
import logging
import sys
import time
import shutil

DATA_DIR = (Path(__file__) / '../../data').resolve()
OLM_FILE: Path = DATA_DIR / 'FactSet_Outlook.olm'
ZIP_BYTES = (0x50, 0x4B, 0x03, 0x04)
BYTEORDER = sys.byteorder


class ZipFileHeader(NamedTuple):
    """
    https://en.wikipedia.org/wiki/Zip_(file_format)#Local_file_header
    +--------+-----+--------------------------------------------------------+
    | offset | len | description                                            |
    +--------+-----+--------------------------------------------------------+
    | 0      | 4   | Local file header signature = 0x04034b50 (little-endian)
    | 4      | 2   | Version needed to extract (minimum)
    | 6      | 2   | General purpose bit flag
    | 8      | 2   | Compression method
    | 10     | 2   | File last modification time
    | 12     | 2   | File last modification date
    | 14     | 4   | CRC-32
    | 18     | 4   | Compressed size
    | 22     | 4   | Uncompressed size
    | 26     | 2   | File name length (n)
    | 28     | 2   | Extra field length (m)
    | 30     | n   | File name
    | 30+n   | m   | Extra field
    +--------+-----+-------------------------------------------------------+
    """
    signature: bytes
    zip_version: bytes
    gen_purpose_bitflag: bytes
    compression_method: bytes
    filemod_time: int
    filemod_date: int
    crc32: bytes
    compressed_size: int
    size: int
    filename_len: int
    extrafield_len: int
    filename: str
    extrafield: bytes


class OlmFileError(Exception):
    pass


def write_zipfile(output_dir: Path, olm_fileobj: BinaryIO, start_offset: int, end_offset: int) -> ZipFileHeader:
    signature = olm_fileobj.read(4)
    if int.from_bytes(signature, 'little') == 0x04034b50:

        zip_version = olm_fileobj.read(2)
        gen_purpose_bitflag = olm_fileobj.read(2)
        compression_method = olm_fileobj.read(2)
        filemod_time = int.from_bytes(olm_fileobj.read(2), 'little')
        filemod_date = int.from_bytes(olm_fileobj.read(2), 'little')
        crc32 = olm_fileobj.read(4)
        compressed_size = int.from_bytes(olm_fileobj.read(4), 'little')
        size = int.from_bytes(olm_fileobj.read(4), 'little')
        filename_len = int.from_bytes(olm_fileobj.read(2), 'little')
        extrafield_len = int.from_bytes(olm_fileobj.read(2), 'little')
        filename = olm_fileobj.read(filename_len).decode('ascii')
        extrafield = olm_fileobj.read(extrafield_len)

        zipfile_header = ZipFileHeader(
            signature, zip_version, gen_purpose_bitflag, compression_method,
            filemod_time, filemod_date, crc32, compressed_size, size,
            filename_len, extrafield_len, filename, extrafield
        )
        output_filepath = output_dir / f'{filename}.zip'
        output_filepath.parent.mkdir(parents=True, exist_ok=True)
        with output_filepath.open('wb') as of:
            of.write(olm_fileobj.read(compressed_size))
        return zipfile_header

    raise OlmFileError(f"There isn't a zipfile at offset {start_offset}")


if __name__ == '__main__':

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    console_loghdlr = logging.StreamHandler(sys.stderr)
    logger.addHandler(console_loghdlr)

    with OLM_FILE.open('rb') as olm:

        zip_extract_dir = DATA_DIR / f'{OLM_FILE}.d'
        if zip_extract_dir.exists():
            shutil.rmtree(zip_extract_dir)
        zip_extract_dir.mkdir()

        zip_bytes_len = len(ZIP_BYTES)
        b = olm.read(1)
        bnum = int.from_bytes(b, BYTEORDER)
        bytes_read = 1
        matched_bytes = 0

        prev_zip_delim = -1
        filenum = 1
        stime = time.time()

        while b:

            if matched_bytes == zip_bytes_len:
                pos = bytes_read - zip_bytes_len - 1
                if prev_zip_delim != -1:
                    with (zip_extract_dir / f'file{filenum}.zip').open('wb') as outfile:
                        olm.seek(prev_zip_delim)
                        # read from prev found zip delimiter to before this one
                        outfile.write(olm.read(pos - prev_zip_delim))
                    filenum += 1
                prev_zip_delim = pos
                matched_bytes = 0

                timing = time.time() - stime
                logger.debug(f'Found zip delimiter at byte {pos} ({round(timing, 4)}s)')
                stime = time.time()

            if bnum == ZIP_BYTES[matched_bytes]:
                matched_bytes += 1
            else:
                matched_bytes = 0

            b = olm.read(1)
            bnum = int.from_bytes(b, BYTEORDER)
            bytes_read += 1

    logger.debug(f'Read {bytes_read} bytes')
    logger.debug(f'Last byte was {hex(bnum)} ({b})')
